<?php
class Animal
{
    public $legs = 2, $cold_bloded = false, $name;

    public function __construct($name = 'nama')
    {
        $this->name = $name;
    }

    public function get_name()
    {
        return $this->name;
    }
    public function get_legs()
    {
        return $this->legs;
    }
    public function get_cold_blooded()
    {
        return $this->cold_bloded ? 'True' : 'False';
    }
}
