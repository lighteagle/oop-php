<?php

require_once 'Animal.php';
require_once 'Ape.php';
require_once 'Frog.php';

$sheep = new Animal("shaun");

echo '--- Release 0 ---<br>';
echo $sheep->get_name(); // "shaun"
echo '<br>';
echo $sheep->get_legs(); // 2
echo '<br>';
echo $sheep->get_cold_blooded(); // false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

echo '<br><br>--- Release 1 ---<br>';

$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

echo '<br>';

$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"